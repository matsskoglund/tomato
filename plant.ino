// Sensor pins
#define waterLevelSensorPower 7
#define waterLevelSensorPin A0
#define valvePin 5
#define relayPower 6

// Value for storing water level
int val = 0;

void setup()
{
  // Set D7 as an OUTPUT
  //pinMode(waterLevelSensorPower, OUTPUT);
  pinMode(relayPower, OUTPUT);
  pinMode(valvePin, OUTPUT);
  // Set to LOW so no power flows through the sensor
  // digitalWrite(waterLevelSensorPower, HIGH);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);
}

float R = 560;      // resistance of resistor used in the circuit in ohms
float v;             // variable to hold sensor reading
//float h;             // fluid height with respect to reference
float ho;            // reference height
int i = 0;           // counter
float level;       // Water level

void loop()
{
//  level = readWaterLevel();
 level = analogRead(A0);
  Serial.println(level);                   // display data
  // make easier to read
  if (level < 330) {
    //  Serial.print("Öppnar ventil. ");
    digitalWrite(relayPower, HIGH);
    digitalWrite(LED_BUILTIN, HIGH);
    digitalWrite(valvePin, HIGH);
  } else if (level > 400) {
    //  Serial.print("Stänger ventil. ");
    digitalWrite(LED_BUILTIN, LOW);
    digitalWrite(valvePin, LOW);
    digitalWrite(relayPower, LOW);
  }
  delay(1000);
}

float readWaterLevel()
{

  float h;
  // put your main code here, to run repeatedly:
              // read sensor
  
 return v;
  /*v = 5 * v / 1023;               // convert sensor reading to voltage

  // measure height for reference
  if ( i == 0)
  {
    ho = (1500 + R * (1 - 5 / v)) / 140;
    i++;
  }

  h = (1500 + R * (1 - 5 / v)) / 140 - ho + 1; // calculate fluid height
  // in inches with respect to reference
  // 1 is activation height

  return h * 2.54 + 0.46 - 3;
*/
}
